import {
  _getJSON,
  _postJSON,
  _putJSON,
  _delete
} from './api.service'

const PVTX_URL = `/cgi-bin/pvtx`

export const Show = async () => _getJSON(`${PVTX_URL}/show`)
export const Begin = async () =>  _postJSON(`${PVTX_URL}/begin`)
export const Abort = async () =>  _postJSON(`${PVTX_URL}/abort`)
export const Commit = async () =>  _postJSON(`${PVTX_URL}/commit`)
export const Add = async (body) => _putJSON(`${PVTX_URL}/add`, body, 'application/x-www-form-urlencoded')
export const Remove = async (part) => 
  _putJSON(`${PVTX_URL}/remove${part ? '?part=' + part : ''}`)
export const Status = async (rev) =>
  _getJSON(`${PVTX_URL}/status${rev ? '?rev=' + rev : ''}`)
export const Run = async (rev) =>
  _postJSON(`${PVTX_URL}/run${rev ? '?rev=' + rev : ''}`)
export const Steps = async (rev) =>
  _getJSON(`${PVTX_URL}/steps${rev ? '?rev=' + rev : ''}`)
