import fetch from 'isomorphic-fetch'

export const _requestContentType = async (
  url,
  contentType = 'application/json',
  method = 'GET',
  body = {},
  token = '',
) => {
  let headers = {
    'Content-Type': contentType
  }

  if (token) headers['Authorization'] = `Bearer ${token}`

  let options = {
    method,
    headers: headers
  }

  if (method !== 'GET' && method !== 'HEAD' && contentType === 'application/json') {
    options['body'] = JSON.stringify(body)
  } else if (method !== 'GET' && method !== 'HEAD') {
    options['body'] = body
  }

  return fetch(url, options)
}

const _requestJSON = async (url, method = 'GET', body = {}, type = 'application/json') => {
  const response = await _requestContentType(
    url,
    type,
    method,
    body
  )
  
  let json = {}
  try {
    json = await response.json()
  } catch (e) { console.error(e) }

  return {
    ok: response.ok,
    redirected: response.redirected,
    headers: response.headers,
    status: response.status,
    json
  }
}

export const _getJSON = async (url) =>
  _requestJSON(url, 'GET')

export const _postJSON = async (url, body, type) =>
  _requestJSON(url, 'POST', body, type)

export const _putJSON = async (url, body, type) =>
  _requestJSON(url, 'PUT', body, type)

export const _patchJSON = async (url, body, type) =>
  _requestJSON(url, 'PATCH', body, type)

export const _delete = async (url) =>
  _requestJSON(url, 'DELETE')

export async function processService (service, success, failure) {
  let resp
  try {
    resp = await service()
    if (!resp.ok) {
      failure(typeof resp.json.error === 'string' ? { code: resp.status, message: resp.json.error } : resp.json)
    } else {
      success(resp.json)
    }
  } catch (err) {
    resp = {
      ok: false,
      json: {
        code: 0,
        message: err
      }
    }
    failure(resp.json)
  }
  return resp
}
