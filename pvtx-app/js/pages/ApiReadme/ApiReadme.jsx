import React, { useRef, useEffect } from 'react'
import ReactMarkdown from 'react-markdown'

import markdown from '../../../content/api-readme.md'
import './apireadme.scss'

export default function ApiReadme() {
  const ref = useRef()
  useEffect(() => {
    if (ref.current && ref.current.querySelectorAll(".hljs").length === 0) {
      window.hljs.highlightAll()
      window.hljs.initLineNumbersOnLoad()
    }
  }, [ref, ref.current])

  return (
    <div className="api-readme js-syntax-highlight dark" ref={ref}>
      <ReactMarkdown>{markdown.replaceAll("__ORIGIN__", window.location.origin)}</ReactMarkdown>
    </div>
  )
}
