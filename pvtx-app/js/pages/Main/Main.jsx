import React, { Component } from "react";
import * as PvtxService from "../../services/pvtx.service";
import DevStatusSummary from "../../components/DevStatusSummary/DevStatusSummary";
import { Button, JSONDisplayWithClipboard } from '../../components/ClipboardFields/ClipboardFields';
import Loading from '../../components/Loading/Loading';
import { ConfirmAction } from '../../components/ConfirmAction/ConfirmAction';
import { Alert } from '../../components/Alert/Alert';
import FileUpload from "../../components/FileUpload/FileUpload";
import ReactDiffViewer from 'react-diff-viewer';

const STATUSES = {
  INITIAL_LOAD: 'INITIAL_LOAD',
  DEPLOYING: 'DEPLOYING',
  UPLOADING: 'UPLOADING'
}

const groupByParts = (json = {}) => {
  return Object.keys(json).reduce((acc, upperKey) => {
    if (upperKey.indexOf("/run.json") >= 0) {
      const k = upperKey.replace("/run.json", "")
      acc[k] = Object.keys(json).reduce((part, key) => {
        if (key.indexOf(k) >= 0) {
          return {
            ...part,
            [key]: json[key]
          }
        }

        return part
      }, {})
    }
    if (upperKey.indexOf("_config/") >= 0) {
      const k = "_config"
      acc[k] = Object.keys(json).reduce((part, key) => {
        if (key.indexOf(k) >= 0) {
          return {
            ...part,
            [key]: json[key]
          }
        }

        return part
      }, {})
    }
    if (upperKey.indexOf("_hostconfig/") >= 0) {
      const k = "_hostconfig"
      acc[k] = Object.keys(json).reduce((part, key) => {
        if (key.indexOf(k) >= 0) {
          return {
            ...part,
            [key]: json[key]
          }
        }

        return part
      }, {})
    }
    return acc
  }, {})
}

function Part({ title, json, onDelete }) {
  return (
    <>
      <div className="d-flex justify-content-between align-items-center">
        <h3>
          {title}
        </h3>
        <ConfirmAction
          title={`Delete ${title}`}
          confirmBtnTxt="Delete this"
          onConfirm={onDelete}
        >
          You are going to delete {title} from your device.
        </ConfirmAction>
      </div>
      <JSONDisplayWithClipboard value={json} />
    </>
  )
}

const Upload = ({ onFile }) => {
  return (
    <div className="d-flex flex-column align-items-center justify-content-center">
      <FileUpload
        onFile={onFile}
        name="parts"
        type="file"
        message="Drag or click to upload new part"
        pictureStyle={{
          cursor: 'pointer',
          width: '400px',
          padding: '1em',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center'
        }}
        style={{
          backgroundColor: 'rgba(0,0,0,0.05)',
          border: 'rgba(0,0,0,0.2) 1px solid',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '1em',
          marginTop: '0',
          overflow: 'hidden'
        }}
      />
    </div>
  )
}

export default class Main extends Component {
  state = {
    deviceStatus: {
      progress: {
        status: "",
        "status-msg": "",
        progress: 0
      },
      rev: "0",
    },
    parts: null,
    rev: '',
    json: null,
    transaction: null,
    error: null,
    loading: STATUSES.INITIAL_LOAD,
    deploying: null,
  }

  componentDidMount() {
    this.syncronize()  
  }

  syncronize = async () => {
    const statusRes = await PvtxService.Status()
    const showRes = await PvtxService.Show()
    const stepsRes = await PvtxService.Steps()

    const update = { loading: false }
    if (showRes.ok) {
      update.parts = groupByParts(showRes.json)
      update.transaction = showRes.json
    }

    if (stepsRes.ok) {
      update.json = stepsRes.json
    }

    if (statusRes.ok) {
      update.deviceStatus = statusRes.json
    }

    if (!statusRes.ok) {
      update.error = statusRes.json?.error 
    }

    if (Object.keys(update).length > 0) {
      this.setState(update)
    }
  }

  getStep = async () => {
    const stepsRes = await PvtxService.Steps()
    if (stepsRes.ok) {
      this.setState({ transaction: stepsRes.json })
    }
  }
 
  begin = async (evnt) => {
    if (this.state.parts) { return }

    this.setState({ transaction: {} })

    const resp = await PvtxService.Begin()
    if (resp.ok) {
      this.setState({ transaction: resp.json, parts: groupByParts(resp.json) })
    } else {
      this.setState({ transaction: null, parts: null })
    }
  }

  remove = (part) => async (evnt) => {
    if (!this.state.parts) { return }

    const resp = await PvtxService.Remove(part)
    if (resp.ok) {
      this.setState({ transaction: resp.json, parts: groupByParts(resp.json) })
    }
  }

  add = async (bin) => {
    this.setState({ loading: STATUSES.UPLOADING })
    
    const update = { loading: null }
    const resp = await PvtxService.Add(bin)
    if (resp.ok) {
      update.transaction = resp.json
      update.parts = groupByParts(resp.json)
    }
    this.setState(update)
  }

  abort = async () => {
    if (!this.state.parts) { return }

    const resp = await PvtxService.Abort()
    if (resp.ok) {
      this.setState({ transaction: null, parts: null })
      this.getStep()
    }
  }

  commit = async (e) => {
    if (!this.state.parts) { return }
    this.setState({ loading: STATUSES.DEPLOYING })

    const resp = await PvtxService.Commit()
    if (!resp.ok) {
      return
    }

    const respRun = await PvtxService.Run(resp.json.revision)
    if (!respRun.ok) {
      return
    }

    this.setState({ transaction: null, loading: null })
  }

  onDevicePull = (status) => {
    if (!status.error && status.rev !== this.state.deviceStatus.rev) {
      this.syncronize()
    }

    if (
      !status.error
      && status.progress
      && status.progress.status
      && this.state.deviceStatus
      && this.state.deviceStatus.progress
      && this.state.deviceStatus.progress.status
      && status.progress.status !== this.state.deviceStatus.progress.status) {
      this.syncronize()
    }
  }

  render () {    
    if (this.state.loading === STATUSES.INITIAL_LOAD) {
      return (<Loading />)
    }

    if (this.state.loading === STATUSES.DEPLOYING) {
      return (
        <>
          <DevStatusSummary
            pull={true}
            initialStatus={this.state.deviceStatus}
            rev={this.state.rev}
            onUpdate={this.onDevicePull}
          />
          <div className="d-flex justify-content-center pt--4 pb--4">
            <h1>Deploying changes...</h1>
            <Loading />
          </div>
        </>
      )
    }
    return (
      <div>
        {this.state.error && (
          <Alert className="alert-danger"> 
            {this.state.error}
          </Alert>
        )}
        <DevStatusSummary
          pull={true}
          initialStatus={this.state.deviceStatus}
          rev={this.state.rev}
          onUpdate={this.onDevicePull}
        />
        {!this.state.parts && (
          <>
            <div className="d-flex justify-content-center pt--4 pb--4">
              <button
                onClick={this.begin}
                className="btn btn-primary btn-lg"
              >
                Begin new transaction
              </button>
            </div>
            <div>
              <h4>Current Step</h4>
              {this.state.json && (
                <JSONDisplayWithClipboard value={this.state.json} />
              )}
            </div>
          </>
        )}
        {this.state.parts && (
          <>
            {this.state.loading === STATUSES.UPLOADING && (
              <div className="d-flex justify-content-center align-items-center flex-column pt--4 pb--4">
                <h1>Uploading...</h1>
                <Loading />
              </div>
            )}
            {this.state.loading !== STATUSES.UPLOADING && (
              <div className="pb--4 pt--4">
                <Upload onFile={this.add} />
              </div>
            )}
            {Object.keys(this.state.parts).map((key) => (
              <Part
                key={key}
                title={key}
                json={this.state.parts[key]}
                onDelete={this.remove(key)}
              />
            ))}
            <div className="pt--2 pb--2">
              <h4 className="pt--2 pb--2">Transaction diff</h4>
              <div className="row">
                <div className="col-md-12 overflow-scroll">
                  <ReactDiffViewer 
                    oldValue={JSON.stringify(this.state.json, "", "  ")}
                    newValue={JSON.stringify(this.state.transaction, "", "  ")}
                    splitView={true}
                  />
                </div>
              </div>
            </div>
            <div className="d-flex justify-content-center pt--4 pb--4">
              <div className="" role="group" aria-label="Basic example">
                <button
                  disabled={this.state.loading === STATUSES.DEPLOYING}
                  onClick={this.abort}
                  className="btn btn-secondary ml--1 mr--1"
                >
                  Abort transaction
                </button>
                <button
                  disabled={this.state.loading === STATUSES.DEPLOYING}
                  onClick={this.commit}
                  className="btn btn-primary ml--1 mr--1"
                >
                  Commit transaction
                </button>
              </div>
            </div>
          </>
        )}
      </div>
    )
  }
}
