import React from 'react';
import { Component } from 'react';
import { Status } from '../../services/pvtx.service';

const emptyFunction = () => {}

export default class DevStatusSummary extends Component {
  state = {
    rev: "",
    progress: {
      progress: "",
      "status-msg": "",
      progress: 0
    }
  }

  static getDerivedStateFromProps(props, state) {
    return {
      ...state,
      ...props.initialStatus,
    }
  }

  constructor(props) {
    super(props)

    this.interval = null
  }

  componentDidMount() {
    this.props.pull ? this.startPulling() : this.stopPulling()
  }

  componentDidUpdate() {
    (this.props.onUpdate || emptyFunction)(this.state)
    this.props.pull ? this.startPulling() : this.stopPulling()
  }

  componentWillUnmount() {
    this.stopPulling()
  }

  startPulling = () => {
    this.stopPulling()
    this.interval = setInterval(async () => {
      const statusRes = await Status()

      if (statusRes.ok) {
        this.setState({ progress: statusRes.json })
      }
    }, 3000)
  }

  stopPulling = () => {
    if (!this.interval) { return }
    clearInterval(this.interval)
    this.interval = null
  }

  render() {
    return (
      <section className="row">
        <div className="col-md-4">
          <div className="">
            <span>Rev: </span>
            <span style={{ fontWeight: 'bold' }}>{this.state.rev}</span>
          </div>
          <div className="">
            <span>Status: </span>
            <span style={{ fontWeight: 'bold' }}>{this.state.progress.status}</span>
          </div>
          <div className="">
            <span>Progress: </span>
            <span style={{ fontWeight: 'bold' }}>{this.state.progress.progress}</span>
          </div>
          {this.state.progress["status-msg"] && this.state.progress["status-msg"] !== "" && (
            <div className="">
              <span>{this.state.progress["status-msg"]}</span>
            </div>
          )}
        </div>
      </section>
    )
  }
}
