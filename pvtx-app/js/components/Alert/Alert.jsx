import React, { useEffect, useState } from 'react'

export function Alert({ autoClose = false, duration = 4000, children, className = '', afterClose = () => {} }) {
  const [open, setOpen] = useState(true)

  useEffect(() => {
    let timeout
    if (autoClose) {
      timeout = setTimeout(() => {
        setOpen(false)
        setTimeout(() => {
          afterClose()
        }, 400)
      }, duration)
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout)
      }
    }
  }, [autoClose])

  const onCloseHandler = () => {
    setOpen(false)
    setTimeout(() => {
      afterClose()
    }, 400)
  }

  return (
    <div
      className={`alert alert-dismissible fade ${open ? 'show' : 'hide'} ${className}`}
      role="alert"
    >
      {children}
      <button
        type="button"
        className="btn-close"
        data-bs-dismiss="alert"
        aria-label="Close"
        onClick={onCloseHandler}
      />
    </div>
  )
}