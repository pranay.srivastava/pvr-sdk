import * as React from 'react'
import { Button } from '../ClipboardFields/ClipboardFields';
import DeleteIcon from '@mui/icons-material/Delete';

export interface IConfirmActionProps {
  onClose: () => void
  onConfirm: () => void
  closeClasses: string
  confirmClases: string
  closeBtnTxt: string
  confirmBtnTxt: string
  btnChildren: React.ReactChildren,
  title: string
  delay: number
}

export const ConfirmAction: React.FunctionComponent<IConfirmActionProps> = (props) => {
  const {
    delay = 400,
    closeBtnTxt = 'Close',
    confirmBtnTxt = 'Confirm',
    closeClasses = 'btn btn-secondary btn-sm',
    confirmClases = 'btn btn-primary btn-sm',
    title = 'Remove',
    onClose = () => {},
    onConfirm  = () => {}
  } = props
  
  const [open, setOpen] = React.useState(false)

  const onCloseHandler = () => {
    setOpen(false)
    const timeout =  setTimeout(() => {
      onClose()
    }, delay)
    return () => {
      clearTimeout(timeout)
    }
  }

  const onConfirmHandler = () => {
    setOpen(false)
    const timeout = setTimeout(() => {
      onConfirm()
    }, delay)
    return () => {
      clearTimeout(timeout)
    }
  }

  if (!open) {
    return (
      <Button
        title={title}
        type="button"
        className="btn btn-sm btn-light btn-link"
        style={{ color: "red" }}
        aria-label="Remove"
        onClick={() => setOpen(true)}
      >
        {props.btnChildren || (<DeleteIcon />)}
      </Button>
    )
  }

  return (
    <div className={`modal fade ${open ? 'show':''}`} tabIndex={-1} style={{ display: open ? 'block' : 'none' }} >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">
              {props.title}
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              onClick={onCloseHandler}
              aria-label="Close"></button>
          </div>
          <div className="modal-body">
            {props.children}
          </div>
          <div className="modal-footer">
            <button
              type="button"
              onClick={onCloseHandler}
              className={closeClasses}
              data-bs-dismiss="modal"
            >
              {closeBtnTxt}
            </button>
            <button
              type="button"
              onClick={onConfirmHandler}
              className={confirmClases}
            >
              {confirmBtnTxt}
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}