import React, { useState, useCallback, useRef } from 'react'
import FileInput from '../FileInput/FileInput'
import './styles.scss'

export default function FileUpload (props) {
  const {
    onChange,
    value,
    message,
    style = {},
    pictureStyle = {},
    onFile = () => {},
    ...rest
  } = props

  const fileUpload = useRef()
  const [state, setState] = useState({
    error: null,
    dragOver: false,
    cropping: false,
    bin: value,
    file: null,
    uploading: false,
  })

  const openUploadDialog = (event) => {
    event.preventDefault()
    if (fileUpload.current) {
      fileUpload.current.click()
    }
  }

  const reader = new window.FileReader()
  reader.onload = async function () {
    setState((s) => ({ ...s, error: null, bin: reader.result }))
    
    onFile(reader.result)
  }

  const onDragOver = (event) => {
    event.preventDefault()
  }

  const onDragleave = (event) => {
    event.preventDefault()
    setState({ ...state, dragOver: false })
  }

  const onDragStart = (event, id) => {
    event.preventDefault()
    setState({ ...state, dragOver: true })
  }

  const onFileHandler = () => {
    const files = fileUpload.current.files
    if (files.length === 0) {
      setState({
        ...state,
        error: 'Please drop a file',
        dragOver: false,
        bin: null
      })

      return
    }

    if (files[0].type.indexOf('application/x-compressed-tar') === -1) {
      setState({
        ...state,
        error: 'You need to upload a compressed tar file from pvr export',
        bin: undefined
      })
      return
    }

    setState({
      ...state,
      dragOver: false,
      file: { name: files[0].name, size: files[0].size, type: files[0].type }
    })
    reader.readAsArrayBuffer(files[0])
  }

  const onDrop = (event) => {
    event.preventDefault()
    const dt = event.dataTransfer
    const files = dt.files
    const newEvent = new window.Event('change')
    fileUpload.current.files = files
    fileUpload.current.dispatchEvent(newEvent)
  }

  return (
    <React.Fragment>
      <FileInput
        {...rest}
        ref={fileUpload}
        error={state.error}
        onChange={onFileHandler}
        className="invisible"
        style={{ height: 0 }}
      />
      {!state.cropping && (
        <React.Fragment>
          <div
            draggable
            style={{ ...style }}
            className={`drap-drop-area ${state.dragOver ? 'highlight' : ''}`}
            onDragOver={onDragOver}
            onDragStart={onDragStart}
            onDragEnter={onDragStart}
            onDragLeave={onDragleave}
            onDrop={onDrop}
          >
            {state.bin && state.bin !== '' && (
              <>
                {JSON.stringify(state.file)}
              </>
            )}
            {!state.bin && (
              <div
                onClick={openUploadDialog}
                style={pictureStyle}
              >
                {message || 'Drag your logo image here'}
              </div>
            )}
            {state.error && (
              <p className="text-danger">
                {state.error}
              </p>
            )}
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  )
}
