import React from "react";

import logo from "../img/logo.svg";
import logoWhite from "../img/logo-white.svg";
import Main from './pages/Main/Main';
import {
  MemoryRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import ApiReadme from "./pages/ApiReadme/ApiReadme";

export default function App() {
  return (
    <Router>
      <header className="header container">
        <div className="row">
          <div className="col-md-6 pt--2 pb--2">
            <img src={logo} height="60" />
          </div>
          <div className="col-md-6 pt--2 pb--2">
            <nav className="nav justify-content-end">
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/"
                exact={true}
              >
                Home
              </NavLink>
              <NavLink
                activeClassName="active"
                className="nav-link"
                to="/app/api-docs"
              >
                API documentation
              </NavLink>
            </nav>
          </div>
        </div>
      </header>
      <section className="main container">
        <div
          id="page-content-wrapper"
          className="col-md-12 pt--2 pb--2"
        >
          <Switch>
            <Route path={["/", "/app/"]} exact={true}>
              <Main />
            </Route>
            <Route path="/app/api-docs">
              <ApiReadme />
            </Route>
          </Switch>
        </div>
      </section>
      <footer className="footer container-fluid bg-dark">
        <div className="container">
          <div className="col-md-12 pt--2 pb--2">
            <p>
              <a href="https://pantavisor.io" target="_blank" rel="nofollow">
                <img src={logoWhite} height="30" />
              </a>
            </p>
            <p>
              Pvtx Frontend
            </p>
          </div>
        </div>
      </footer>
    </Router>
  );
}
