# PVR-SDK

PVR-SDK container to mantain your pantavisor system by using our main tooling called pantabox.

Because in pantavisor everything runs as a container, even our own tools and platform are containers running on top of pantavisor.

### RUN on development

run on the device

```
socat -d -d TCP-LISTEN:3307,reuseaddr,fork UNIX-CONNECT:/pantavisor/pv-ctrl
```

build the container

```
docker build . -t pvr-sdk -f Dockerfile.amd64
```

and run it with some volumens and configuration

```
docker run -v /sys/fs/cgroup -v /run -v ${PWD}/files/pvtx-www/cgi-bin:/pvtx-www/cgi-bin -v ${PWD}/demodata/usr/bin/pv-socat:/usr/bin/pv-socat -p 12378:12378 --rm --name pvr-sdk pvr-sdk
```