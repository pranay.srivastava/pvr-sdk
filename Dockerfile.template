FROM alpine as qemu

RUN if [ -n "${QEMU_ARCH}" ]; then \
		wget -O /qemu-${QEMU_ARCH}-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-${QEMU_ARCH}-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-${QEMU_ARCH}-static; \
	fi; \
	chmod a+x /qemu-${QEMU_ARCH}-static

FROM node as node-builder

WORKDIR /app
COPY pvtx-app /app

COPY pvtx-app/entrypoint /app/entrypoint
RUN yarn && yarn build

FROM golang:1.13-alpine as pvr-build

ENV GO111MODULES=on

COPY gomodules /build/gomodules
COPY files /build/files
COPY .git /build/.git
COPY .gitmodules /build/.gitmodules

WORKDIR /build/gomodules/pvr

RUN apk update; apk add git
RUN pwd && ls -la && version=`git describe --tags` && sed -i "s/NA/$version/" version.go
RUN CGO_ENABLED=0 GOOS=linux GOARCH=${CROSS} go build -v .
RUN find .

FROM ruby:slim as docs-builder

WORKDIR /docs
COPY reference /docs/reference/
COPY man_builder.sh /docs/man_builder.sh

RUN apt-get update; \
	apt-get install -y make groff gcc ruby ruby-dev; \
	gem install ronn; \
	./man_builder.sh ./man

FROM ${ARCH}/busybox:latest as busybox

FROM registry.gitlab.com/asacasa/alpine-busybox-master:${ARCH}

COPY --from=qemu /qemu-${QEMU_ARCH}-static /usr/bin/

RUN apk update; \
	apk add --update \
		openrc \
		ca-certificates \
		wget \
		jq \
		dropbear \
		dropbear-openrc \
		dropbear-scp \
		dropbear-ssh \
		lxc \
		curl \
		logrotate; \
	rc-update add dropbear default; \
	sed -i '/DROPBEAR_OPTS/c DROPBEAR_OPTS="-p :22"' /etc/conf.d/dropbear; \
	mv /etc/periodic/daily/logrotate /etc/periodic/hourly/; \
	rm -rf /var/cache/apk/*; \
	rm /etc/init.d/networking

RUN apk add \
	dialog \
	squashfs-tools \
	tmux \
	ncurses \
	socat \
	sudo \
	man-db \
	nano && \
	rm -rf /var/cache/apk/*

ADD files /
ADD files_pantabox /

RUN rc-update add zeronetworking default && \
	rc-update add pvr-auto-follow default && \
	rc-update add pv-socat default && \
	rc-update add pv-user-meta-sync default && \
	rc-update add pv-httpd default && \
	mkdir -p /root/.ssh && \
	echo root:pantabox | chpasswd && \
	adduser -g root --gecos "" --disabled-password -s /bin/sh pantavisor && \
	mkdir -p /home/pantavisor/.ssh && \
	mkdir -p /etc-volume && \
	mkdir /usr/share/man/man1 && \
	echo "pantavisor ALL=(ALL) ALL" > /etc/sudoers.d/pantavisor && chmod 0440 /etc/sudoers.d/pantavisor && \
	echo pantavisor:pantabox | chpasswd

VOLUME [ "/home/pantavisor/.ssh" ]
VOLUME [ "/var/pvr-sdk" ]
VOLUME [ "/etc/dropbear" ]
VOLUME [ "/etc-volume" ]

WORKDIR /workspace
COPY --from=pvr-build /build/gomodules/pvr/pvr /usr/bin/
COPY --from=docs-builder /docs/man /usr/share/man/man1
COPY --from=node-builder /app/app /www/app
COPY ./LICENSE /usr/share/docs/pvr-sdk/LICENSE

RUN pvr global-config AutoUpgrade=false && \
	mandb

ENV PVR_DISABLE_SELF_UPGRADE=true
ENV PVR_CONFIG_DIR=/var/pvr-sdk/.pvr

CMD [ "/sbin/init" ]
