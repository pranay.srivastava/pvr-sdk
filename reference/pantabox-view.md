pantabox-view -- view configuration the details for all running containers in your system
===============

## SYNOPSIS

`pantabox-view`

## DESCRIPTION

View the configuration of a container, including all of the fields inside the `src.json`, the configuration volumes, the type of LXC overlay, Pantavisor arguments and source configurations.

## EXAMPLES

`pantabox-view`

```
Module Overview: nginx

Source (Docker)
- Name: nginx:latest
- Digest: nginx@sha256:1c70a669bbf07f9862f269162d776c35144b116938d1becb4e4676270cff8f75

Storage
- lxc-overlay: boot

Args
PV_RUNLEVEL: app

Source Configs
{}

Config Files
- /etc/nginx/nginx.conf
```

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.