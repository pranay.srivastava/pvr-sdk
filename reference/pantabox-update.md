pantabox-update -- update any container running in your system
=================

## SYNOPSIS

`pantabox-update`

## DESCRIPTION

Update any container running in your system. The command downloads the must recent version of the container using the specified tag and container source URL in the `src.json` kept inside the container configuration folder.

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.