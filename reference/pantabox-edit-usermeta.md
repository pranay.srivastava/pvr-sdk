pantabox-edit-usermeta -- edit pantavisor user-meta data
======================

## SYNOPSIS

`pantabox-edit-usermeta`

## DESCRIPTION

This command allows you to edit the user-meta data used by Pantavisor Linux. The user-meta data is safe in a `.json` format of key:value

Pantavisor creates files with the key name and value from the `.json` file and keeps them inside the user-meta folder.

Those files reside at: `/pantavisor/user-meta/key`

## EXAMPLES

To view an example:

- Run `pantabox-edit-usermeta` to bring up the nano editor.

![nano editor for user-meta](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/e810f62550ee98cdbaa1d083aa376524bf4dc7a4/images/user-meta01.png)

- Edit the `.json` file by adding your user-meta key and values.

![edit user-meta json file](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/e810f62550ee98cdbaa1d083aa376524bf4dc7a4/images/user-meta02.png)

- Confirm you changes.

![confirm and save](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/e810f62550ee98cdbaa1d083aa376524bf4dc7a4/images/user-meta03.png)

- View them in the filesystem as files in the `/pantavisor/user-meta` folder

![how you see the user-meta](https://gitlab.com/pantacor/pv-platforms/pvr-sdk/-/raw/e810f62550ee98cdbaa1d083aa376524bf4dc7a4/images/user-meta04.png?inline=false)

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.