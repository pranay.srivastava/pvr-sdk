pantabox-install -- install a new container in your system
=======================================

## SYNOPSIS

`pantabox-install`

## DESCRIPTION

This allows you to install new containers onto your Pantavisor system. You can install containers from the following three places:

1. By directly downloading a Docker container using the Docker name and tag of the container.
2. From another Pantavisor-enabled device kept in the cloud.
3. From a tarball that's been exported from Pantavisor-enabled device. 

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.