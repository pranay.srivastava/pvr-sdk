pantabox-info -- get information about your pantavisor system
==============

## SYNOPSIS

`pantabox-info`

## DESCRIPTION

Shows you information about the system, including the mode that Pantavisor is running, the device-meta data, the network interfaces and the user-meta data.

## EXAMPLES

`pantabox-info`

The output is shown inside a dialog, but the content and is similar to the following:

```
https://pantavisor.io

Mode:
local

Network Interfaces:
 lo.ipv4: 127.0.0.1
 eth0.ipv4: 192.168.68.111
 lxcbr0.ipv4: 10.0.3.1
 lo.ipv6: ::1
 eth0.ipv6: fe80::ac03:84d:458a:c434%eth0
 tailscale0.ipv6: fe80::6448:b31d:8176:d81c%tailscale0

Device Info:
 pantahub.claimed: 0
 pantahub.online: 0
 pantavisor.mode: local
 pantavisor.revision: locals/pbox-1626456113
 pantavisor.dtmodel: Raspberry Pi 4 Model B Rev 1.2
 pantavisor.version: 014-rc8-13-ga9eb1b6-210708-1808ba3
 pantavisor.arch: aarch64/64/EL

User Meta:
 testing.meta: this is the content of my new meta data
```

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.