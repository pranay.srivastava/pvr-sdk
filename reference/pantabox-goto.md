pantabox-goto -- run a specific revision on your device
================

## SYNOPSIS

`pantabox-goto` [REVISION_NUMBER]

## DESCRIPTION

Tells Pantavisor to switch between device revisions. This operation restarts all of the containers with the selected state of the revision.

## EXAMPLES

SOME EXAMPLES HERE

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.