pantabox-make-factory -- convert the running revision into your initial revision
=======================

## SYNOPSIS

`pantabox-make-factory`

## DESCRIPTION

Convert the running revision into the initial state of the device and then switch the device to remote mode. This operation should be run on a unclaimed device or one that has never connected to a hub cloud service.

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.