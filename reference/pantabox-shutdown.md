pantabox-shutdown -- shutdown your system
===================

## SYNOPSIS

`pantabox-shutdown` [message]

## DESCRIPTION

Sends a signal to Pantavisor to shutdown your system.

## EXAMPLES

SOME EXAMPLES HERE

## COPYRIGHT

Pantabox is Copyright  (c) 2021 by Pantacor Ltd.