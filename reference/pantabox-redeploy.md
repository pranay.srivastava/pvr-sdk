pantabox-redeploy -- deploy your current state to a Pantavisor compatible cloud
===================

## SYNOPSIS

`pantabox-redeploy`

## DESCRIPTION

Deploys the current state to a Pantavisor compatible cloud. If the revision already exists in the cloud, it will be redeployed.

## EXAMPLES

## COPYRIGHT

pantabox is Copyright  (c) 2021 by Pantacor Ltd.