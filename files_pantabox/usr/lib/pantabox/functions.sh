#!/bin/sh

installed_rev=
configfile=/etc/pvr-sdk/config.json
TMPPDIR=${TMPPDIR:-/var/pvr-sdk/tmp/}

PVR_OBJECTS_DIR=/var/pvr-sdk/pvr/objects
[ -d "$PVR_OBJECTS_DIR" ] || mkdir -p $PVR_OBJECTS_DIR
export PVR_OBJECTS_DIR

mktempp() {
	mkdir -p $TMPPDIR
	mktemp -p $TMPPDIR $@
}

local_ph_host() {
    if [ -f "$configfile" ]; then
        addr=`cat $configfile | jq -r ".httpd.listen"`
        addr=${addr#null}
        port=`cat $configfile | jq -r ".httpd.port"`
        port=${port#null}
    fi

    addr=${addr:-127.0.0.1}
    port=${port:-12368}

    echo "http://${addr}:${port}/cgi-bin"
}

needs_root_permission() {
    if [ "$(id -u)" -ne 0 ]; then
        echo "You need to be root or run this command using sudo."
        exit 0
    fi
}

select_part() {
	statef=$1

	run_jsons=`cat $statef  | jq -r ". | to_entries[]  | select ( .key | endswith(\"run.json\")) | .key"`

	apps=
	for f  in $run_jsons; do
		appname=${f%%/run.json}
		apps="$appname $apps"
	done

	select=`dialog --stdout --colors --no-items --title "Pantabox View" --menu "Select a Pantavisor Part" 0 0 10 $apps` || break

	echo $select
}

install_local_objects() {
    _newstate=$1

    # applying to new draft version
    sh -c "cd $_newstate; pvr json | jq -r 'to_entries[] | select ( .key | endswith(\".json\") | not) | .value'  | sort -u| grep -v pantavisor" \
	| sh -c "set -x; while read -r object; do \
		pvcontrol objects put $PVR_OBJECTS_DIR/\$object \$object || true; \
		done; set +x; sleep 2" 2>&1 | dialog --progressbox "Installing Objects ..." 40 80
}

install_local_json() {

    json=$1

    if [ -n "$2" ]; then
	_rev=locals/$2
    else
	_rev=locals/pbox-`date +%s`
    fi

    sh -c "cd $_newstate; set -x; pvcontrol steps put $json ${_rev}; set +x; sleep 2" 2>&1 | \
		dialog --progressbox "Installing New State ..." 40 80

    installed_rev=${_rev}
}

install_local() {

    _newstate=$1

    if [ -n "$2" ]; then
	_rev=locals/$2
    else
	_rev=locals/pbox-`date +%s`
    fi

    install_local_objects $_newstate

    # until pantavisor allows to overload 'drating' states we will create new unique
    install_local_json $_newstate/.pvr/json
}

run_installed() {

    _rev=${1:-$installed_rev}
    if [ -n "$_rev" ]; then
    
	if dialog --yesno "New Revision ${_rev} installed!\n\nDo you want to run it now?" 0 0; then
            echo "Starting new revision ${_rev} in 5 seconds. Use Ctrl-C to abort..."
            sleep 6
            pvcontrol commands run ${_rev}
	else
            echo "Note: you can apply this new revision later with pantabox command run ${_rev}"
	fi
    else
	echo "No installed revision known or provided. Cannot run_installed..."
	exit 1
    fi
}

wait_for_revision() {

	rev=$1
	cur=
	maxsleep=${2:-30}
	maxsettle=${3:-45}
	j=
	ok=
	dialog --title "Installation Progress" --infobox "Waiting for revision \"$rev\" to start ..." 20 60
	for i in `seq 1 $maxsleep`; do
		cur=`pvcontrol devmeta list | jq -r ".[\"pantavisor.revision\"]"`
		if [ "$cur" = "$rev" ]; then
			dialog --infobox "Done! " 20 60
			break
		fi
		cur=
		dialog --infobox "Waiting for revision \"$rev\" to start ..." 20 60
		sleep 1
	done

	if ! [ "$cur" = "$rev" ]; then
		dialog --title "Installation failed" --infobox "
			Failed to start revision \"$rev\" in $maxsleep seconds.
			To debug inspect: /pantavisor/logs/$rev/pantavisor/pantavisor.log
		" 20 60
		exit 1
	fi

	dialog --title "Installation Progress" --infobox "Waiting for revision \"$rev\" to finish testing  ..." 20 60
	for i in `seq 1 $maxsettle`; do
		status=`pvcontrol steps show-progress current | jq -r .status`
		if [ "$status" = "DONE" ] || [ "$status" = "UPDATED" ]; then
			echo " [OK - $status]"
			dialog --title "Installation finished" --infobox "[OK - $status]" 20 60
			ok=yes
			break
		fi
		dialog --title "Installation Progress" --infobox "Waiting for revision \"$rev\" to finish testing  ..." 20 60
		sleep 1
	done

	if [ -z "$ok" ]; then
		dialog --title "Installation failed" --infobox "
			Failed to start revision \"$rev\" in $maxsettle seconds.
			To Debug inspect /pantavisor/logs/$rev/pantavisor/pantavisor.log
		" 20 60
		exit 1
	fi
}

