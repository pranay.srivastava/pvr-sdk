#!/bin/sh

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"` 

# source functions
. $dir/../lib/pantabox/functions.sh

claimed=`pvcontrol devmeta list | jq -r '.["pantahub.claimed"]'`

if [ "$claimed" == "0"]; then
	clear
  echo "Your device needs to be claimed first. Run: "
	echo -e "\npantabox-make-factory\n"
	echo "and then"
	echo -e "\npantabox-claim\n"
  exit 1
fi

if dialog --yesno "Publish your current system state to Pantacor Hub and switch Pantavisor to remote mode.\n\nThis deploys your current working state as a new remote revision on Pantacor Hub and switches back to the most recent remote revision.\n\nThis operation might reboot your system!\n\nDo you want to continue?" 0 0; then

	clear
	deviceid=
	if [ ! -s /pantavisor/device-id ]; then
		if [ -s /storage/config/pantahub.config ]; then
			deviceid=`cat /storage/config/pantahub.config | grep creds.id | sed -e 's/creds\.id=//;s/ .*//'`
		fi
		if [ -s /storage/config/unclaimed.config ]; then
			deviceid=`cat /storage/config/unclaimed.config | grep creds.id | sed -e 's/creds\.id=//;s/ .*//'`
		fi
	else
		deviceid=`cat /pantavisor/device-id`
	fi

	if [ -z "$deviceid" ]; then
		echo "Only devices previously registered with Pantacor Hub are able to go in remote mode.\n\n. Try pantabox-goto to install a remote revision and register with Pantacor Hub to enable this feature."
		exit 1
	fi

	pantahubhost=`cat /pantavisor/pantahub-host | sed -e 's/:443//'`
	if [ -z "$pantahubhost" ]; then
		echo "Only devices previously registered with Pantacor Hub are able to go in remote mode.\n\n. Try pantabox-goto to install a remote revision and register with Pantacor Hub to enable this feature"
		exit 1
	fi

	rev=`$dir/pvcontrol devmeta list | jq -r ".[\"pantavisor.revision\"]"`

	if [ -z "$rev" ]; then
		echo "Could not identify current running system state; Report this as a bug."
		exit 2
	fi

	trailpath=/storage/trails/$rev

	if ! [ -d $trailpath ]; then
		echo "Could not find the running system state $trailpath on disk; Report this as a bug."
		exit 3
	fi

	message=$(\
		dialog --inputbox "Commit message" 0 0 "pantabox-goremote published local revision $rev" \
		3>&1 1>&2 2>&3 3>&- \
	)

	clear

	# check if we have access key
	access_key=`cat ~/.pvr/auth.json  | jq -r ".tokens | to_entries | .[] | select (.key | startswith(\"$pantahubhost\")) | .value | .[\"access-token\"]"`

	# if not ask user to give us a new key
	while [ -z "$access_key" ]; do
		pvr login $pantahubhost/auth/auth_status
		access_key=`cat ~/.pvr/auth.json  | jq -r ".tokens | to_entries | .[] | select (.key | startswith(\"$pantahubhost\")) | .value | .[\"access-token\"]"`
	done

	# now we PUT our usrmeta
	pvcontrol usrmeta list | curl -s -XPUT -T "-" -H "Content-Type: application/json" \
		-H "Authorization: Bearer $access_key" \
		$pantahubhost/devices/$deviceid/user-meta

	sh -c "cd /storage/trails/$rev; pvr post -m \"$message\" $pantahubhost/trails/$deviceid"

	# XXX: later also filter by revision that is in state DONE
	remoterev=`pvcontrol steps list | jq -r ".[].name | select ( . | startswith(\"locals/\") | not )" | sort -n -r | head -n 1`
	echo "Will go remote and enter revision ${rev} as ${remoterev} in 5 seconds. Abort with Ctrl-C"
	sleep 5
	$dir/pvcontrol commands run ${remoterev}
	echo "Thanks ... "
	wait_for_revision ${remoterev}
fi

