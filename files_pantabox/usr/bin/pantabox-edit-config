#!/bin/sh

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"`

. $dir/../lib/pantabox/functions.sh

PVCTRL=$dir/pvcontrol

$PVCTRL steps get current > $tmpdir/state.json

part=`select_part $tmpdir/state.json`

if [ -z "$part" ]; then
	exit 0
fi

tmpdir=`mktempp -d -t pantabox-edit--config--$part--XXXXXXXXX`
draftdir=$tmpdir/draft
pristdir=$tmpdir/pristine
mkdir $draftdir

sh -c "cd $draftdir && pvr init && pvr get $(local_ph_host)#_config/$part && pvr checkout" 2>&1 | dialog --progressbox "Retrieving selected part ..." 40 80
cp -r $draftdir $pristdir

mkdir -p $draftdir/_config/$part

cd $draftdir

clear
while true; do

cat << EOF

===============================
Pantabox Interactive Edit Configs
===============================

We provisioned the following files
for the parts you selected to edit:

`find _config/$part -type f | while read -r line; do echo "    \$line"; done`

You can now edit part \'$part\' and customize them.

Remember to pvr add and pvr commit the changes and apply them to your system.

When complete, you can:
1. Apply committed changes: exit 0 (or Ctrl-D)
2. Discard changes: exit 1

For help visit: https://pantavisor.io

EOF

	HOME=$tmpdir PS1="pvctlbox::$part# " sh

	if dialog --yesno "
Do you want to post the changes you made?

Your changes will be lost if you don't save them. In order to save those changes click [no] and do \"pvr add; pvr commit\"

Files status:
`pvr add; pvr status`
" 0 0; then
		pvr checkout
		break
	else
		continue
	fi
done
cd -

# Applying changes ...

$PVCTRL steps get current | jq "[ to_entries[] | select ( .key | startswith(\"_config/$part/\") | not) ] | from_entries" \
	> $tmpdir/state.json

cat $tmpdir/state.json $draftdir/.pvr/json | jq -s "[.[]] | .[0] * .[1]" > $tmpdir/newstate.json

# installing objects

install_local_objects $draftdir

if dialog --yesno "Do you want to apply this new state?

`cat $tmpdir/newstate.json | jq .`

" 0 0; then

	install_local_json $tmpdir/newstate.json

	run_installed
	wait_for_revision $installed_rev
fi

