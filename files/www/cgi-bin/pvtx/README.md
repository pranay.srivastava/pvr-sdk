PVTX Rest API
====================

PVTX is a tool to manage pantavisor devices using only tar files exported from PVR CLI. Allowing to manage without using any cloud service or docker container sources.

This rest api exposes the same actions availables on the pvtx CLI.

## Endpoints

The api is available by default on `http://DEVICE_IP:12378/cgi-bin/` the access is public it doesn't need password or credentials.

### show

The show endpoint show the current transaction running on pvtx. If doesn't exist any transaction will return and error.

```bash
curl http://localhost:12378/cgi-bin/pvtx/show
```

If doesn't have any transaction started will return `HTTP/1.1 400 Bad Request` with this body

```json
{
  "error": "No active transaction.",
  "transaction": null
}
```

If there is a transaction will return the full state json of that transaction like this one.

Example response:
```json
{
  "#spec": "pantavisor-service-system@1",
  "_config/pvr-sdk/etc/pvr-sdk/config.json": {
    "httpd": {
      "listen": "0.0.0.0",
      "port": "12368"
    }
  },
  "_hostconfig/pvr/docker.json": {
    "platforms": [
      "linux/arm64",
      "linux/arm"
    ]
  },
  "bsp/addon-plymouth.cpio.xz4": "beae6a7bb235916cac52bcfece64c30615cded8c4c640e6941e7ecabe53b4920",
  "bsp/build.json": {
    "altrepogroups": "",
    "branch": "master",
    "commit": "7d5ba78761e4d880c3403d642187bfdc93e49683",
    "gitdescribe": "014-rc9-12-g7d5ba78",
    "pipeline": "355967648",
    "platform": "rpi64",
    "project": "pantacor/pv-manifest",
    "pvrversion": "pvr version 022-15-g26ebb342",
    "target": "arm-rpi64",
    "time": "2021-08-19 15:08:44 +0000"
  },
  "bsp/firmware.squashfs": "c968a674d12258f00f4d9251637065a04abf7f95285308bfca7e4f6ccf9de7c5",
  "bsp/kernel.img": "b59438e4cb0db11689601e7e26e8dc6dad0b5007072edbf10e886a8dd51d2397",
  "bsp/modules.squashfs": "385f04fca555912f8655018f97cd09a2502fdd79afc14cd5fd57682d0a2cf4e0",
  "bsp/pantavisor": "405f907a1d1d086d89808c6fc691bf7d169e6e38b24c0110c7b950c4dda2742d",
  "bsp/run.json": {
    "addons": [
      "addon-plymouth.cpio.xz4"
    ],
    "firmware": "firmware.squashfs",
    "initrd": "pantavisor",
    "initrd_config": "",
    "linux": "kernel.img",
    "modules": "modules.squashfs"
  },
  "bsp/src.json": {
    "#spec": "bsp-manifest-src@1",
    "pvr": "https://pvr.pantahub.com/pantahub-ci/arm_rpi64_bsp_latest#bsp"
  },
  "network-mapping.json": {},
  "pvr-sdk/lxc.container.conf": "a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca",
  "pvr-sdk/root.squashfs": "896d66166794aa11f14ffe3c8fddc80a0563d85bae086d25dce69836d8eb0468",
  "pvr-sdk/root.squashfs.docker-digest": "35e1d5180e95445a7effdd44e8ef09405e2051d6c50c6b71965469fabc768368",
  "pvr-sdk/run.json": {
    "#spec": "service-manifest-run@1",
    "config": "lxc.container.conf",
    "name": "pvr-sdk",
    "root-volume": "root.squashfs",
    "storage": {
      "docker--etc-dropbear": {
        "persistence": "permanent"
      },
      "docker--etc-volume": {
        "persistence": "permanent"
      },
      "docker--home-pantavisor-.ssh": {
        "persistence": "permanent"
      },
      "docker--var-pvr-sdk": {
        "persistence": "permanent"
      },
      "lxc-overlay": {
        "persistence": "boot"
      }
    },
    "type": "lxc",
    "volumes": []
  },
  "pvr-sdk/src.json": {
    "#spec": "service-manifest-src@1",
    "args": {
      "PV_LXC_EXTRA_CONF": "lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0",
      "PV_SECURITY_WITH_STORAGE": "yes"
    },
    "config": {},
    "docker_digest": "registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:95b5d63a216773af9c8b2b82e25e2e9d40a049bae9c62cd2308eff5feb9b32dd",
    "docker_name": "registry.gitlab.com/pantacor/pv-platforms/pvr-sdk",
    "docker_source": "remote,local",
    "docker_tag": "arm32v6",
    "persistence": {},
    "template": "builtin-lxc-docker"
  },
  "storage-mapping.json": {},
}
```

### begin

The begin endpoint start a new transaction only if there is not a trasaction started. If a transaction is already started this is going to fail.

```bash
curl -X POST http://localhost:12378/cgi-bin/pvtx/begin
```

If there is not error will return 200 with the full state response (the same seem on show)

Response:
```json
{
  # ...FULL TRANSACTION
}
```

If there is an error will get this response

```json
{
  "error": "No active transaction.",
  "transaction": { ...FULL TRANSACTION }
}
``` 

### abort

This will abort the current transation running. If there is not transaction running is going to fail.

```bash
curl -X POST http://localhost:12378/cgi-bin/pvtx/abort
```

If everything is ok you will get a 200 OK response. If not an error similar to this.

```json
{
  "error": "No active transaction. Start a transaction first.",
  "transaction": null
}
```

### add

This add and update parts using the tar file result of a `pvr export` command.

```bash
curl -X PUT http://localhost:12378/cgi-bin/pvtx/add --data-binary @exported.tgz
```

This will fail if there is not transaction started or fails to add that exported file.

### remove?part=PART_NAME

Remove any part of the current transaction. Using the example here on the show command we can see it has several parts, we could delete one by name, example: `pvr-sdk`.

```bash
curl -X PUT http://localhost:12378/cgi-bin/pvtx/remove?part=pvr-sdk
```

That will return the now state without the `pvr-sdk` part.

Could give you an error in case it fails.

```json
{ "error": "ERROR_MESSAGE_STRING" }
```

### commit

The commit endpoint allows to commit all the changes in the transaction and returns a revision ID that could be now run.

```bash
curl -X POST http://localhost:12378/cgi-bin/pvtx/commit
```

Response:
```json
{
  "revision": "locals/pvtx-1636485481-42c76d97"
}
```

If there is not transaction to commit you will get an error.

```json
{
  "error": "No active transaction. Start a transaction first.",
  "transaction": null
}
```

### run?rev=REVISION_ID

Runs allow you to run any revision on the device.

```
curl -X POST http://localhost:12378/cgi-bin/pvtx/run?rev=locals/pvtx-1636485481-42c76d97
```

This will return the progress of that revision when success

```json
{
  "status": "TESTING",
  "status-msg": "Awaiting to see if update is stable",
  "progress": 95
}
```

### status?rev=REVISION_ID

Return the progress of a revision. The rev parameter will default to current.

```bash
curl http://localhost:12378/cgi-bin/pvtx/status
```

Response
```json
{
  "rev": "REVISION_ID", 
  "progress": {
    "status": "UPDATED",
    "status-msg": "Update finished, revision not set as rollback point",
    "progress": 100
  }
}
```

or 

```bash
curl http://localhost:12378/cgi-bin/pvtx/status?rev=locals/pvtx-1636485481-42c76d97
```

Response:
```json
{
  "rev": "locals/pvtx-1636485481-42c76d97", 
  "progress": {
    "status": "UPDATED",
    "status-msg": "Update finished, revision not set as rollback point",
    "progress": 100
  }
}
```
