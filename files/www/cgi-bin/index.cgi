#!/bin/sh

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"`

uri=${REQUEST_URI##/cgi-bin/}

if [ "$uri" = $REQUEST_URI ]; then
	echo "Status: 500 Internal Server Error"
	echo
	exit
fi

case ${uri} in
	objects*)
		PATH_INFO="${REQUEST_URI##/cgi-bin/objects}" $dir/objects
		;;
	state*)
		PATH_INFO="${REQUEST_URI##/cgi-bin/state}" $dir/state
		;;
	pvr*)
		PATH_INFO="${REQUEST_URI##/cgi-bin/pvr}" $dir/pvr
		;;
	
esac
		
