#!/bin/sh

set -e

cmd=$0
dir=`sh -c "cd $(dirname $cmd); pwd"`
cmd=`basename $0`
pvtx_dir=$PREFIX/var/pvr-sdk/pvtx
PVCONTROL=${PVCONTROL:-$dir/pvcontrol}

usage() {
    echo "USAGE: $cmd begin|add|remove|abort|commit <positional arguments>"
    echo "   Transactional updates with pvrexport ingest - good for small disks with no object duplication"
    echo "   - begin: start a tx with fresh copy of current working state"
    echo "   - add: ingest a pvrexport tarball adding or replacing existing elements"
    echo "   - remove: remove all elements of a part of the state json"
    echo "   - abort: abort and trigger run of GC to clear unused objects"
    echo "   - commit: apply working draft"
    echo "   - show: display current working draft json"
    exit 0
}

echo_e() {
    echo $@ 1>&2
}

touchit () {
    : >> $1
}

cmd_begin() {
    if [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: active transaction; finish your work with 'commit' or 'abort' first ..."
	return
    fi

    rm -rf $pvtx_dir/*
    $PVCONTROL steps get current >  $pvtx_dir/state.json
    cat $pvtx_dir/state.json | JSON.sh 1 > $pvtx_dir/state.JSON.sh.1
    cp $pvtx_dir/state.JSON.sh.1 $pvtx_dir/state.JSON.sh.1.draft
}

cmd_abort() {
    rm -rf $pvtx_dir/*
    # pvcontrol commands rungc
}

_make_json_draft() {
    echo -n '{'
    cat $pvtx_dir/state.JSON.sh.1.draft | while read -r line; do
	if ! [ "$_first" = "" ]; then
	    echo -n ","
	fi
	_first=no
	echo -n "$line" | sed -e 's/^\[\([^[:space:]]*\)\][[:space:]].*$/\1/'
	echo -n ":"
	echo -n "$line" | sed -e 's/^\[[^[:space:]]*\][[:space:]]//'
    done
    echo -n '}'
}

cmd_commit() {
    if ! [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: no active transaction"
	return
    fi
    _make_json_draft > $pvtx_dir/draft.json
    hashv=`cat $pvtx_dir/draft.json | sha256sum | sed 's/[[:space:]].*$//'`
    vers="locals/pvtx-`date +%s`-`echo $hashv | head -c 8`"
    pvcontrol steps put  $pvtx_dir/draft.json $vers
    echo "$vers"
    rm -rf $pvtx_dir/*
}

# cmd_add - add a pvrexport to state json (and replace existing)
#           This operation requires an active transcation (see begin command)
#           1. json will be inspected; if not a valid format
#              the add will be aborted
#           2. objects will get uploaded to PV
#           3. json will be patched into state.json
cmd_add() {
    if ! [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: no active transaction"
	return 1
    fi

echo asdas:
    if ! cat $1 | tar -xz --to-command "$dir/pvtx ingest"; then
	echo_e "ERROR: failed to ingest provided pvr export tarball"
	return 2
    fi
echo asdas:
}

cmd_remove() {
    if ! [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: no active transaction"
	return 3
    fi

    part=$1
    if [ -z "$part" ]; then
	echo_e "ERROR: must remove a part (missing paramter)"
	return 4
    fi

    # filter part ... simple
    cat $pvtx_dir/state.JSON.sh.1.draft | while read -r line; do
	if ! echo $line | grep -q "^\[\"$part/" && ! echo $line | grep -q "^\[\"_config/$part/" ; then
	    echo $line
	fi
    done > $pvtx_dir/state.JSON.sh.1._draft
    
    mv $pvtx_dir/state.JSON.sh.1._draft $pvtx_dir/state.JSON.sh.1.draft
}

_check_ingest_spec() {
    # we only succeed of spec matches ...
    cat $pvtx_dir/ingest.JSON.sh.1 | grep ^\\[\"\\#spec\"\] | grep -q \""$1"\"
}

_merge_JSON_sh() {
    # first lets strip all keys that $2 has so we dont get dupes
    cat $2 > $pvtx_dir/merge.2
    cat $1 | while read -r line; do
        key=`echo $line | sed -e 's/\["\([^[:space:]]*\)"\][[:space:]].*$/\1/'`
        if ! grep -q "\[\"$key\"\]" $pvtx_dir/merge.2; then
            echo $line
        fi
    done > $pvtx_dir/merge.1

    # now we merge through concat and sort
    cat $pvtx_dir/merge.1 $pvtx_dir/merge.2 | busybox sort -s | busybox uniq
}

# ingest a pvr export tarball elements; invoked through tar --to-command
cmd_ingest() {
    if ! [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: no active transaction"
	return 5
    fi

    if [ -f $pvtx_file/ingest_failed ]; then
	return 6
    fi

    case "$TAR_FILENAME" in
	json)
	    cat | JSON.sh 1 > $pvtx_dir/ingest.JSON.sh.1
	    if ! _check_ingest_spec "pantavisor-service-system@1"; then
	        touchit $pvtx_dir/ingest_failed
	        echo_e "Not supported spec:"
	        cat $pvtx_dir/ingest.JSON.sh.1 | grep ^\\[\"\\#spec\"\] 1>&2
		return 7
	    fi

	    # merge JSON.sh
	    _merge_JSON_sh $pvtx_dir/state.JSON.sh.1.draft $pvtx_dir/ingest.JSON.sh.1 > $pvtx_dir/state.JSON.sh.1._draft
	    mv $pvtx_dir/state.JSON.sh.1._draft $pvtx_dir/state.JSON.sh.1.draft
	    echo "Merged JSON:"; cat $pvtx_dir/state.JSON.sh.1.draft
	    ;;
	objects/*)
	    sha=${TAR_FILENAME#objects/}
	    if ! $PVCONTROL objects put -${TAR_SIZE} $sha; then
		echo_e "ERROR: putting object with sha \"$sha\""
	        touchit $pvtx_dir/ingest_failed
		return 8
	    fi
	    :
	    ;;
	*)
	    touchit $pvtx_dir/ingest_failed
	    echo_e "Illegal file in tarball: $TAR_FILENAME"
	    return 9
    esac
}

cmd_show() {
    if ! [ -f $pvtx_dir/state.JSON.sh.1.draft ]; then
	echo_e "ERROR: no active transaction"
	return 10
    fi
    _make_json_draft
}

one=$1

if [ -z "$one" ]; then
    echo_e "ERROR: no argument 1; see --help"
    exit 1
fi

shift 1

if [ "$one" = "--help" ]; then
    usage
    exit 0
fi

if ! [ -d $pvtx_dir ]; then mkdir -p $pvtx_dir; fi

case "$one" in
    abort)
	cmd_abort $@
	;;
    add)
	cmd_add $@
	;;
    begin)
	cmd_begin $@
	;;
    commit)
	cmd_commit $@
	;;
    ingest)
	cmd_ingest $@
	;;
    remove)
	cmd_remove $@
	;;
    show)
	cmd_show $@
	;;
    --help)
	usage
	;;
    *)
	echo_e "ERROR: unknown first argument $one; see --help"
	exit 100
	;;
esac
    
